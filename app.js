const apiCallFromRequest = require('./Request')
const apiCallFromNode = require('./NodeJsCall')

const http = require('http')

/////set function/////

function setvalue() {
    try {
        // contract Abi defines all the variables,constants and functions of the smart contract. replace with your own abi
        var abi = [
   {
       "constant": false,
       "inputs": [
         {
             "name": "x",
             "type": "uint256"
         }
       ],
       "name": "set",
       "outputs": [],
       "payable": false,
       "stateMutability": "nonpayable",
       "type": "function"
   },
   {
       "constant": true,
       "inputs": [],
       "name": "get",
       "outputs": [
         {
             "name": "",
             "type": "uint256"
         }
       ],
       "payable": false,
       "stateMutability": "view",
       "type": "function"
   }
        ]
        //contract address. please change the address to your own
        var contractaddress = '0xc80cae7c51f27bc25b3862d072d56fa84965f5c1';
        //instantiate and connect to contract address via Abi
        var myAbi = web3.eth.contract(abi);
        // var myAbi = new web3.eth.Contract(abi);
        var myfunction = myAbi.at(contractaddress);
        //call the set function of our SimpleStorage contract
        myfunction.set.sendTransaction(abi, { from: web3.eth.accounts[0], gas: 4000000 }, function (error, result) {
            if (!error) {
                console.log(result);
            } else {
                console.log(error);
            }
        })
    } catch (err) {
        console.log(err);
    }
 }

/////set function end//////

http.createServer((req, res) => {
        if(req.url === "/request"){
            apiCallFromRequest.callApi(function(response){
                //console.log(JSON.stringify(response));
                res.write(JSON.stringify(response));
                setvalue();

                res.end();
            });
        }
        else if(req.url === "/node"){
            apiCallFromNode.callApi(function(response){
                res.write(response);
                res.end();
            });
        }
        
        // res.end();
}).listen(3000);

console.log("service running on 3000 port....");